using Ignis.Unity.Test.Tools.Tables;
using NUnit.Framework;

namespace Ignis.Unity.Test.Tools.Tests.Tables
{
    public sealed class ValueAlignTest
    {
        [Test]
        public void TestLeft()
        {
            Assert.That(ValueAlign.Left.Write("abc", 5),
                Is.EqualTo("abc  "));
        }

        [Test]
        public void TestRight()
        {
            Assert.That(ValueAlign.Right.Write("abc", 5),
                Is.EqualTo("  abc"));
        }

        [TestCase(123, "  123")]
        [TestCase(1.24d, " 1.24")]
        [TestCase(1.25f, " 1.25")]
        [TestCase((byte)126, "  126")]
        public void NumbersAreAlignedToRight(object number, string expected)
        {
            var target = ValueAlign.From(number);

            Assert.That(target.Write(number, 5),
                Is.EqualTo(expected));
        }
    }
}