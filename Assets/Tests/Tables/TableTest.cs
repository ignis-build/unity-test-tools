using Ignis.Unity.Test.Tools.Tables;
using NUnit.Framework;

namespace Ignis.Unity.Test.Tools.Tests.Tables
{
    public sealed class TableTest
    {
        private Table _target;

        [SetUp]
        public void SetUp()
        {
            _target = new Table();
        }

        [Test]
        public void OneRow()
        {
            _target.AddBody("foo", "bar");

            Assert.That(_target.Write(), Is.EqualTo(@"
foo | bar
".Trim()));
        }

        [Test]
        public void AlignPositions()
        {
            _target.AddBody("foo", "bar");
            _target.AddBody("foo1", "b");

            Assert.That(_target.Write(), Is.EqualTo(@"
foo  | bar
foo1 | b  
".Trim()));
        }

        [Test]
        public void NumbersAreAlignedToRight()
        {
            _target.AddBody("pass", 1);
            _target.AddBody("fail", 10);

            Assert.That(_target.Write(), Is.EqualTo(@"
pass |  1
fail | 10
".Trim()));
        }

        [Test]
        public void TestFooter()
        {
            _target.AddBody("pass", 1);
            _target.AddBody("fail", 9);

            _target.AddFooter("total", 10);

            Assert.That(_target.Write(), Is.EqualTo(@"
pass  |  1
fail  |  9
----- | --
total | 10
".Trim()));
        }

        [Test]
        public void TestColumnValues()
        {
            _target.AddBody("pass", 1);
            _target.AddFooter("fail", 9);

            Assert.That(_target.ColumnValues(0),
                Is.EquivalentTo(new[] { "pass", "fail" }));
        }

        [Test]
        public void TestMaxValueCount()
        {
            _target.AddBody("row1", 1);
            _target.AddFooter("row2", 2, true);

            Assert.That(_target.MaxValueCountByRow(), Is.EqualTo(3));
        }
    }
}