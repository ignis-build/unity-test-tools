using System;
using Ignis.Unity.Test.Tools.Times;

namespace Ignis.Unity.Test.Tools.Tests.Files
{
    public sealed class MockTimeProvider : TimeProvider
    {
        private readonly DateTimeOffset _offset;

        public MockTimeProvider(DateTimeOffset offset)
        {
            _offset = offset;
        }

        public override DateTimeOffset GetLocalNow()
        {
            return _offset;
        }
    }
}