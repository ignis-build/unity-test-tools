using System;
using Ignis.Unity.Test.Tools.Files;
using NUnit.Framework;

namespace Ignis.Unity.Test.Tools.Tests.Files
{
    public sealed class TicksFileNameResolverTest
    {
        [Test]
        public void TestResolve()
        {
            var offset = DateTimeOffset.Parse("2021/10/12T01:23:45");

            var target = new TicksFileNameResolver(new MockTimeProvider(offset));

            Assert.That(target.Resolve(),
                Is.EqualTo($"TestResults-junit-{offset.Ticks}.xml"));
        }
    }
}