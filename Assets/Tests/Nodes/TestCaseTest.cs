using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;
using TestCase = Ignis.Unity.Test.Tools.Nodes.TestCase;

namespace Ignis.Unity.Test.Tools.Tests.Nodes
{
    public sealed class TestCaseTest
    {
        private MockTestResult _testCase;

        [SetUp]
        public void SetUp()
        {
            _testCase = TestCase("Test1").WithDuration(0.982);
            TestSuite("TestClass1", "My.TestClass1").WithChildren(_testCase);
        }

        [Test]
        public void TestPass()
        {
            var target = TestCase.From(_testCase.Passed());

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"" />
".Trim()));
        }

        [Test]
        public void TestFail()
        {
            var target = TestCase.From(_testCase
                .Failed("Assertion error message", "Call stack printed here"));

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"">
  <failure message=""Assertion error message"">Call stack printed here</failure>
</testcase>
".Trim()));
        }

        [Test]
        public void TestSkip()
        {
            var target = TestCase.From(_testCase.Skipped());

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"">
  <skipped />
</testcase>
".Trim()));
        }

        [Test]
        public void TestInconclusive()
        {
            var target = TestCase.From(_testCase.Inconclusive());

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"" />
".Trim()));
        }
    }
}