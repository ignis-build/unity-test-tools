﻿using System.Xml.Linq;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using UnityEditor.TestTools.TestRunner.Api;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;
using TestCase = Ignis.Unity.Test.Tools.Nodes.TestCase;
using TestSuite = Ignis.Unity.Test.Tools.Nodes.TestSuite;

namespace Ignis.Unity.Test.Tools.Tests.Nodes
{
    public sealed class TestSuiteTest
    {
        private MockTestResult _testCase1;
        private MockTestResult _testCase2;
        private MockTestResult _testCase3;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = TestCase("Test1").WithDuration(0.982).Passed();
            _testCase2 = TestCase("Test2").WithDuration(0.102).Passed();
            _testCase3 = TestCase("Test3").WithDuration(0.103).Passed();
        }

        [Test]
        public void TestSimple()
        {
            var testResult =
                TestAssembly("Foo.Bar.dll").WithId("1028")
                    .WithCounts(3, 1, 4, 2)
                    .WithDuration(0.0335219).WithStartTime("2023-10-30T08:26:08")
                    .WithChildren(
                        TestSuite("TestClass1", "Foo.Bar").WithChildren(
                            _testCase1
                        ));

            var target = TestSuite.From(testResult);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuite name=""Foo.Bar.dll"" tests=""10"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
  {Xml(_testCase1)}
</testsuite>
".Trim()));
        }

        [Test]
        public void TestFlatten()
        {
            var testResult =
                TestAssembly("Foo.Bar.dll").WithId("1028")
                    .WithCounts(3, 1, 10, 2)
                    .WithDuration(0.0335219).WithStartTime("2023-10-30T08:26:08")
                    .WithChildren(
                        TestSuite("Foo").WithChildren(
                            TestSuite("Bar").WithChildren(
                                TestSuite("TestClass1", "Foo.Bar.TestClass1")
                                    .WithChildren(
                                        _testCase1,
                                        _testCase2
                                    )),
                            TestSuite("TestClass2", "Foo.TestClass2").WithChildren(
                                _testCase3
                            )));

            var target = TestSuite.From(testResult);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuite name=""Foo.Bar.dll"" tests=""16"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
  {Xml(_testCase1)}
  {Xml(_testCase2)}
  {Xml(_testCase3)}
</testsuite>
".Trim()));
        }

        private static XElement Xml(ITestResultAdaptor testCase)
        {
            return TestCase.From(testCase).Xml();
        }
    }
}