using System.Xml.Linq;
using Ignis.Unity.Test.Tools.Nodes;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using UnityEditor.TestTools.TestRunner.Api;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;
using Enumerable = System.Linq.Enumerable;
using TestCase = Ignis.Unity.Test.Tools.Nodes.TestCase;

namespace Ignis.Unity.Test.Tools.Tests.Nodes
{
    public sealed class TestSuitesTest
    {
        private MockTestResult _testCase1;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = TestCase("Test1").WithDuration(0.892).Passed();
        }

        [Test]
        public void TestSimple()
        {
            var result = TestSuite("my-test").WithChildren(
                TestAssembly("Foo.Bar.dll").WithId("1028")
                    .WithCounts(3, 1, 10, 2)
                    .WithDuration(0.0335219).WithStartTime("2023-10-30T08:26:08")
                    .WithChildren(
                        TestSuite("Class1", "Class1").WithChildren(
                            _testCase1)
                    ));

            var target = TestSuites.From(result);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuites>
  <testsuite name=""Foo.Bar.dll"" tests=""16"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
    {Xml(_testCase1)}
  </testsuite>
</testsuites>
".Trim()));
        }

        [Test]
        public void TestMultipleAssemblies()
        {
            var result = TestSuite("my-test").WithChildren(
                TestAssembly("Foo.Bar.dll").WithId("1028")
                    .WithCounts(3, 1, 10, 2)
                    .WithDuration(0.0335219).WithStartTime("2023-10-30T08:26:08")
                    .WithChildren(
                        TestSuite("Class1", "Class1").WithChildren(
                            _testCase1
                        )),
                TestAssembly("Test.dll").WithId("1029")
                    .WithCounts(1, 3, 4, 2)
                    .WithDuration(0.101).WithStartTime("2023-10-30T10:10:10")
                    .WithChildren(Enumerable.Empty<MockTestResult>())
            );

            var target = TestSuites.From(result);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuites>
  <testsuite name=""Foo.Bar.dll"" tests=""16"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
    {Xml(_testCase1)}
  </testsuite>
  <testsuite name=""Test.dll"" tests=""10"" skipped=""2"" failures=""3"" errors=""0"" time=""0.101"" timestamp=""2023-10-30T10:10:10"" id=""1029"" package=""Test.dll"" />
</testsuites>
".Trim()));
        }

        private static XElement Xml(ITestResultAdaptor testCase)
        {
            return TestCase.From(testCase).Xml();
        }
    }
}