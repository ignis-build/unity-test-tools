﻿using System.Linq;
using Ignis.Unity.Test.Tools.Nodes;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;

namespace Ignis.Unity.Test.Tools.Tests.Nodes
{
    public sealed class EnumerableTest
    {
        private MockTestResult _testCase1;
        private MockTestResult _testCase2;
        private MockTestResult _testCase3;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = TestCase("Test1").WithDuration(0.982).Passed();
            _testCase2 = TestCase("Test2").WithDuration(0.102).Passed();
            _testCase3 = TestCase("Test3").WithDuration(0.103).Passed();
        }

        [Test]
        public void TestTestCases()
        {
            var target = TestAssembly("Foo.Bar.dll").WithChildren(
                TestSuite("Foo").WithChildren(
                    TestSuite("Bar").WithChildren(
                        TestSuite("TestClass1").WithChildren(
                            _testCase1,
                            _testCase2
                        )),
                    TestSuite("TestClass2").WithChildren(
                        _testCase3
                    )));

            var actual = target.Children.TestCases()
                .Select(result => result.Test.Name)
                .ToArray();

            Assert.That(actual, Is.EquivalentTo(new[] { "Test1", "Test2", "Test3" }));
        }
    }
}