using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;

namespace Ignis.Unity.Test.Tools.Tests.Reporting
{
    public sealed class JUnitReporterTest
    {
        [Test]
        public void TestJUnit()
        {
            var source = TestSuite("my-test").WithChildren(
                TestAssembly("test.dll").WithId("128")
                    .WithCounts(1, 0, 0, 0)
                    .WithDuration(0.0335219).WithStartTime("2023-10-30T08:26:08")
                    .WithChildren(
                        TestSuite("TestClass1", "TestClass1").WithChildren(
                            TestCase("TestCase1").WithDuration(0.3).Passed()
                        )));
            var file = new MockReportFile();

            var target = new JUnitReporter(file);

            target.RunFinished(source);

            Assert.That(file.ToString(), Is.EqualTo(@"
<?xml version=""1.0"" encoding=""utf-8""?>
<testsuites>
  <testsuite name=""test.dll"" tests=""1"" skipped=""0"" failures=""0"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""128"" package=""test.dll"">
    <testcase name=""TestCase1"" classname=""TestClass1"" time=""0.3"" />
  </testsuite>
</testsuites>
".Trim()));
        }
    }
}