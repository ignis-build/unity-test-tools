using System.Text;
using Ignis.Unity.Test.Tools.Outputs;

namespace Ignis.Unity.Test.Tools.Tests.Reporting.Mocks
{
    internal sealed class MockOutput : IOutput
    {
        private readonly StringBuilder _builder = new();

        public void Log(string message)
        {
            _builder.AppendLine(message);
        }

        public void WriteToConsole(string message)
        {
            _builder.Append(message);
        }

        public override string ToString()
        {
            return _builder.ToString().Trim();
        }
    }
}