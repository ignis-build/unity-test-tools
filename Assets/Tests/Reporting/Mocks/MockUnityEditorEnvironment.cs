using System.Text;
using Ignis.Unity.Test.Tools.Environments;

namespace Ignis.Unity.Test.Tools.Tests.Reporting.Mocks
{
    internal sealed class MockUnityEditorEnvironment : IUnityEditorEnvironment
    {
        private readonly StringBuilder _builder = new();
        public bool IsBatchMode { get; private set; }

        public void Exit(int returnValue)
        {
            _builder.AppendLine($"EditorApplication.Exit({returnValue})");
        }

        public string Log()
        {
            return _builder.ToString().Trim();
        }

        public void BatchMode()
        {
            IsBatchMode = true;
        }

        public void NonBatchMode()
        {
            IsBatchMode = false;
        }
    }
}