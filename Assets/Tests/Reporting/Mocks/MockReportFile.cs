using System.IO;
using System.Text;
using Ignis.Unity.Test.Tools.Files;

namespace Ignis.Unity.Test.Tools.Tests.Reporting.Mocks
{
    internal sealed class MockReportFile : IReportFile
    {
        private MemoryStream _stream;

        public TextWriter CreateWriter()
        {
            _stream?.Dispose();
            _stream = new MemoryStream();

            return new StreamWriter(_stream);
        }

        public override string ToString()
        {
            if (_stream == null) return "null";

            return Encoding.UTF8.GetString(_stream.ToArray());
        }
    }
}