using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using TestStatus = UnityEditor.TestTools.TestRunner.Api.TestStatus;

namespace Ignis.Unity.Test.Tools.Tests.Reporting.Mocks
{
    internal sealed class MockTestResult : ITestResultAdaptor
    {
        private MockTest _test;

        private MockTestResult()
        {
        }

        public TNode ToXml()
        {
            throw new NotSupportedException();
        }

        public ITestAdaptor Test => _test;
        public string Name => throw new NotSupportedException();
        public string FullName => throw new NotSupportedException();
        public string ResultState => throw new NotSupportedException();
        public TestStatus TestStatus { get; private set; }
        public double Duration { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime => throw new NotSupportedException();
        public string Message { get; private set; }
        public string StackTrace { get; private set; }
        public int AssertCount => throw new NotSupportedException();
        public int FailCount { get; private set; }
        public int PassCount { get; private set; }
        public int SkipCount { get; private set; }
        public int InconclusiveCount { get; private set; }
        public bool HasChildren => throw new NotSupportedException();

        public IEnumerable<ITestResultAdaptor> Children { get; private set; } =
            Enumerable.Empty<ITestResultAdaptor>();

        public string Output => throw new NotSupportedException();

        public static MockTestResult TestSuite(string name, string fullName = null)
        {
            return new MockTestResult
            {
                _test = MockTest.TestSuite(name, fullName)
            };
        }

        #region factory

        public static MockTestResult TestAssembly(string name)
        {
            return new MockTestResult
            {
                _test = MockTest.TestAssembly(name)
            };
        }

        public static MockTestResult TestCase(string name)
        {
            return new MockTestResult
            {
                _test = MockTest.TestCase(name)
            };
        }

        #endregion

        #region setters

        public MockTestResult WithId(string id)
        {
            _test = _test.WithId(id);
            return this;
        }

        public MockTestResult WithCounts(int passCount,
            int failCount,
            int inconclusiveCount,
            int skipCount)
        {
            PassCount = passCount;
            FailCount = failCount;
            InconclusiveCount = inconclusiveCount;
            SkipCount = skipCount;
            _test = _test.WithTestCaseCount(passCount + failCount + inconclusiveCount + skipCount);

            return this;
        }

        public MockTestResult WithDuration(double duration)
        {
            Duration = duration;
            return this;
        }

        public MockTestResult WithChildren(IEnumerable<MockTestResult> children)
        {
            Children = children
                .Select(child => child.WithParent(this))
                .ToArray();
            return this;
        }

        public MockTestResult WithChildren(params MockTestResult[] children)
        {
            return WithChildren(children.AsEnumerable());
        }

        public MockTestResult WithStartTime(string startTime)
        {
            StartTime = DateTime.Parse(startTime);
            return this;
        }

        private MockTestResult WithParent(ITestResultAdaptor parent)
        {
            _test = _test.WithParent(parent.Test);
            return this;
        }

        #endregion

        #region status

        public MockTestResult Passed()
        {
            TestStatus = TestStatus.Passed;
            return this;
        }

        public MockTestResult Failed(string message = null, string stackTrace = null)
        {
            TestStatus = TestStatus.Failed;
            Message = message;
            StackTrace = stackTrace;
            return this;
        }

        public MockTestResult Inconclusive()
        {
            TestStatus = TestStatus.Inconclusive;
            return this;
        }

        public MockTestResult Skipped()
        {
            TestStatus = TestStatus.Skipped;
            return this;
        }

        #endregion
    }
}