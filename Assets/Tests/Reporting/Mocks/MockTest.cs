using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using RunState = UnityEditor.TestTools.TestRunner.Api.RunState;

namespace Ignis.Unity.Test.Tools.Tests.Reporting.Mocks
{
    internal sealed class MockTest : ITestAdaptor
    {
        private MockTest()
        {
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string FullName { get; private set; }
        public int TestCaseCount { get; private set; }
        public bool HasChildren => throw new NotSupportedException();
        public bool IsSuite { get; private set; }
        public IEnumerable<ITestAdaptor> Children => throw new NotSupportedException();
        public ITestAdaptor Parent { get; private set; }
        public int TestCaseTimeout => throw new NotSupportedException();
        public ITypeInfo TypeInfo => throw new NotSupportedException();
        public IMethodInfo Method => throw new NotSupportedException();
        public string[] Categories => throw new NotSupportedException();
        public bool IsTestAssembly { get; private set; }
        public RunState RunState => throw new NotSupportedException();
        public string Description => throw new NotSupportedException();
        public string SkipReason => throw new NotSupportedException();
        public string ParentId => throw new NotSupportedException();
        public string ParentFullName => throw new NotSupportedException();
        public string UniqueName => throw new NotSupportedException();
        public string ParentUniqueName => throw new NotSupportedException();
        public int ChildIndex => throw new NotSupportedException();
        public TestMode TestMode => throw new NotSupportedException();


        #region factory

        public static MockTest TestSuite(string name, string fullName)
        {
            return new MockTest
            {
                Name = name,
                FullName = fullName,
                IsSuite = true,
                IsTestAssembly = false
            };
        }

        public static MockTest TestAssembly(string name)
        {
            return new MockTest
            {
                Name = name,
                IsSuite = true,
                IsTestAssembly = true
            };
        }

        public static MockTest TestCase(string name)
        {
            return new MockTest
            {
                Name = name,
                IsSuite = false,
                IsTestAssembly = false
            };
        }

        #endregion

        #region setters

        public MockTest WithId(string id)
        {
            Id = id;
            return this;
        }

        public MockTest WithParent(ITestAdaptor parent)
        {
            Parent = parent;
            return this;
        }

        public MockTest WithTestCaseCount(int testCaseCount)
        {
            TestCaseCount = testCaseCount;
            return this;
        }

        #endregion
    }
}