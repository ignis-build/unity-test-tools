using System.Collections.Generic;
using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;

namespace Ignis.Unity.Test.Tools.Tests.Reporting
{
    public sealed class DotReporterTest
    {
        private MockOutput _output;
        private DotReporter _target;

        [SetUp]
        public void SetUp()
        {
            _output = new MockOutput();
            _target = new DotReporter(_output);
        }

        [Test]
        public void TestDotStyle()
        {
            var results = new[]
            {
                TestSuite("test"),
                TestAssembly("assembly1"),
                TestSuite("TestClass1"),
                TestCase("Test1").Passed(),
                TestCase("Test2").Passed(),
                TestSuite("TestClass2"),
                TestCase("Test3").Passed()
            };

            AllRunFinished(results);

            Assert.That(_output.ToString(), Is.EqualTo(@"
...
".Trim()));
        }

        [Test]
        public void SurroundWithLineBreaks()
        {
            var results = new[]
            {
                TestCase("Test1").Passed()
            };

            _output.WriteToConsole("first");
            AllRunFinished(results);
            _output.WriteToConsole("last");
            
            Assert.That(_output.ToString(), Is.EqualTo(@"
first
.
last
".Trim()));
        }

        [Test]
        public void TestResultStatus()
        {
            var results = new[]
            {
                TestCase("Test1").Passed(),
                TestCase("Test2").Failed(),
                TestCase("Test3").Inconclusive(),
                TestCase("Test4").Skipped()
            };
            
            AllRunFinished(results);
            
            Assert.That(_output.ToString(), Is.EqualTo(@"
.FIS
".Trim()));
        }

        private void AllRunFinished(IEnumerable<MockTestResult> results)
        {
            _target.RunStarted(null);
            foreach (var result in results) _target.TestFinished(result);
            _target.RunFinished(null);
        }
    }
}