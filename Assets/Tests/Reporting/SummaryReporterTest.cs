using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;

namespace Ignis.Unity.Test.Tools.Tests.Reporting
{
    public sealed class SummaryReporterTest
    {
        [Test]
        public void TestSummary()
        {
            var output = new MockOutput();
            var target = new SummaryReporter(output);

            target.RunFinished(TestSuite("test")
                .WithCounts(1, 2, 3, 4));

            Assert.That(output.ToString(), Is.EqualTo(@"
Test finished.

Passed       |  1
Failed       |  2
Inconclusive |  3
Skipped      |  4
------------ | --
Total        | 10
".Trim()));
        }
    }
}