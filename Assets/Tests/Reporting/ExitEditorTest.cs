using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;

namespace Ignis.Unity.Test.Tools.Tests.Reporting
{
    public sealed class ExitEditorTest
    {
        private MockUnityEditorEnvironment _environment;
        private ExitEditor _target;

        private static MockTestResult AllGreen { get; } = TestSuite("test")
            .WithCounts(4, 0, 0, 0);
        private static MockTestResult Failure { get; } = TestSuite("test")
            .WithCounts( 3, 1, 0, 0);

        [SetUp]
        public void SetUp()
        {
            _environment = new MockUnityEditorEnvironment();
            _target = new ExitEditor(_environment);
        }

        [Test]
        public void TestExit()
        {
            _environment.BatchMode();

            _target.RunFinished(AllGreen);

            Assert.That(_environment.Log, Is.EqualTo("EditorApplication.Exit(0)"));
        }

        [Test]
        public void TestExitFailure()
        {
            _environment.BatchMode();

            _target.RunFinished(Failure);

            Assert.That(_environment.Log(), Is.EqualTo("EditorApplication.Exit(1)"));
        }

        [Test]
        public void ShouldNotExit_IfNotInBatchMode_EvenIfFails()
        {
            _environment.NonBatchMode();

            _target.RunFinished(AllGreen);

            Assert.That(_environment.Log(), Is.Empty);
        }
    }
}