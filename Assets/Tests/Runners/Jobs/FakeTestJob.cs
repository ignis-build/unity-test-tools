using System;
using System.Collections.ObjectModel;
using System.Threading;
using Ignis.Unity.Test.Tools.Runners;
using Unity.Jobs;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal readonly struct FakeTestJob
    {
        private static readonly Random Generator = new();
        private readonly ICallbacks _callbacks;
        private readonly ReadOnlyCollection<TestCase> _testCases;
        private readonly FakeTestJobData _jobData;

        public FakeTestJob(ICallbacks callbacks,
            ReadOnlyCollection<TestCase> testCases,
            int jobNumber)
        {
            _callbacks = callbacks;
            _testCases = testCases;
            _jobData = new FakeTestJobData(jobNumber);
        }

        public void Execute()
        {
            var test = new FakeTestAdapter(Generator.Next().ToString(), _testCases);
            
            Thread.Sleep(100);

            _callbacks.RunStarted(test);

            foreach (var testCase in _testCases)
            {
                Thread.Sleep(10);
                testCase.Execute();
            }

            Thread.Sleep(100);
            _callbacks.RunFinished(new FakeTestResultAdapter(test));
        }

        public static FakeTestJob Create(WaitTestCallbacks waiter, ReadOnlyCollection<TestCase> testCases)
        {
            return FakeTestJobs.Instance.Create(waiter, testCases);
        }

        public void Schedule()
        {
            _jobData.Schedule();
        }
    }
}