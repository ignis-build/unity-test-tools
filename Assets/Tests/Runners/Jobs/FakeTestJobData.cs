using Unity.Jobs;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal readonly struct FakeTestJobData : IJob
    {
        private int Number { get; }

        public FakeTestJobData(int number)
        {
            Number = number;
        }

        public void Execute()
        {
            var job = FakeTestJobs.Instance.Get(Number);
            job.Execute();
        }
    }
}