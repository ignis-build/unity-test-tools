using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal sealed class FakeTestJobs
    {
        private readonly Dictionary<int, FakeTestJob> _jobs = new();
        private int _currentNumber;

        public static FakeTestJobs Instance { get; } = new();

        public FakeTestJob Create(ICallbacks callbacks, 
            ReadOnlyCollection<TestCase> testCases)
        {
            var newNumber = NewNumber();
            var job = new FakeTestJob(callbacks, testCases, newNumber);
            _jobs.Add(newNumber, job);
            return job;
        }

        public FakeTestJob Get(int number)
        {
            return _jobs[number];
        }

        private int NewNumber()
        {
            _currentNumber++;
            return _currentNumber;
        }
    }
}