using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ignis.Unity.Test.Tools.Runners;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal sealed class FakeTestRunnerApi : ITestRunnerApi
    {
        private readonly ReadOnlyCollection<TestCase> _testCases;
        private readonly WaitTestCallbacks _waiter;

        public FakeTestRunnerApi(WaitTestCallbacks waiter, IEnumerable<TestCase> testCases)
        {
            _waiter = waiter;
            _testCases = testCases.ToList().AsReadOnly();
        }

        public void Execute(ExecutionSettings settings)
        {
            var job = FakeTestJob.Create(_waiter, _testCases);
            job.Schedule();
        }
    }
}