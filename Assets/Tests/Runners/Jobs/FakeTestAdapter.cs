using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using RunState = UnityEditor.TestTools.TestRunner.Api.RunState;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal sealed class FakeTestAdapter : ITestAdaptor
    {
        private static readonly Random Generator = new();

        public FakeTestAdapter(string id, ReadOnlyCollection<TestCase> testCases)
        {
            Id = id ?? Generator.Next().ToString();
            TestCaseCount = testCases.Count;
        }

        public string Id { get; }
        public string Name => throw new NotSupportedException();
        public string FullName => throw new NotSupportedException();
        public int TestCaseCount { get; }
        public bool HasChildren => throw new NotSupportedException();
        public bool IsSuite => throw new NotSupportedException();
        public IEnumerable<ITestAdaptor> Children => throw new NotSupportedException();
        public ITestAdaptor Parent => throw new NotSupportedException();
        public int TestCaseTimeout => throw new NotSupportedException();
        public ITypeInfo TypeInfo => throw new NotSupportedException();
        public IMethodInfo Method => throw new NotSupportedException();
        public string[] Categories => throw new NotSupportedException();
        public bool IsTestAssembly => throw new NotSupportedException();
        public RunState RunState => throw new NotSupportedException();
        public string Description => throw new NotSupportedException();
        public string SkipReason => throw new NotSupportedException();
        public string ParentId => throw new NotSupportedException();
        public string ParentFullName => throw new NotSupportedException();
        public string UniqueName => throw new NotSupportedException();
        public string ParentUniqueName => throw new NotSupportedException();
        public int ChildIndex => throw new NotSupportedException();
        public TestMode TestMode => throw new NotSupportedException();
    }
}