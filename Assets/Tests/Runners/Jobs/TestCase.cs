namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal sealed class TestCase
    {
        public bool IsCompleted { get; private set; }

        public void Execute()
        {
            IsCompleted = true;
        }
    }
}