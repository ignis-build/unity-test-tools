using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using TestStatus = UnityEditor.TestTools.TestRunner.Api.TestStatus;

namespace Ignis.Unity.Test.Tools.Tests.Runners.Jobs
{
    internal sealed class FakeTestResultAdapter : ITestResultAdaptor
    {
        public FakeTestResultAdapter(ITestAdaptor test)
        {
            Test = test;
        }

        public TNode ToXml()
        {
            throw new NotSupportedException();
        }

        public ITestAdaptor Test { get; }
        public string Name => throw new NotSupportedException();
        public string FullName => throw new NotSupportedException();
        public string ResultState => throw new NotSupportedException();
        public TestStatus TestStatus => throw new NotSupportedException();
        public double Duration => throw new NotSupportedException();
        public DateTime StartTime => throw new NotSupportedException();
        public DateTime EndTime => throw new NotSupportedException();
        public string Message => throw new NotSupportedException();
        public string StackTrace => throw new NotSupportedException();
        public int AssertCount => throw new NotSupportedException();
        public int FailCount => throw new NotSupportedException();
        public int PassCount => throw new NotSupportedException();
        public int SkipCount => throw new NotSupportedException();
        public int InconclusiveCount => throw new NotSupportedException();
        public bool HasChildren => throw new NotSupportedException();
        public IEnumerable<ITestResultAdaptor> Children => throw new NotSupportedException();
        public string Output => throw new NotSupportedException();
    }
}