using Ignis.Unity.Test.Tools.Runners;
using Ignis.Unity.Test.Tools.Tests.Reporting.Mocks;
using NUnit.Framework;
using static Ignis.Unity.Test.Tools.Tests.Reporting.Mocks.MockTestResult;


namespace Ignis.Unity.Test.Tools.Tests.Runners
{
    public sealed class TestResultContextTest
    {
        private MockUnityEditorEnvironment _environment;

        private static MockTestResult AllGreen { get; } = TestSuite("test")
            .WithCounts(4, 0, 0, 0);

        private static MockTestResult Failure { get; } = TestSuite("test")
            .WithCounts(3, 1, 0, 0);

        [SetUp]
        public void SetUp()
        {
            _environment = new MockUnityEditorEnvironment();
        }

        [Test]
        public void TerminateNormallyWhenTestsPassInBatchmode()
        {
            _environment.BatchMode();
            var target = new TestResultContext(AllGreen);

            target.ExitEditorIfBatchmode(_environment);

            Assert.That(_environment.Log(), Is.EqualTo("EditorApplication.Exit(0)"));
        }

        [Test]
        public void TerminateFailsWhenTestsPassInBatchmode()
        {
            _environment.BatchMode();
            var target = new TestResultContext(Failure);

            target.ExitEditorIfBatchmode(_environment);

            Assert.That(_environment.Log(), Is.EqualTo("EditorApplication.Exit(1)"));
        }

        [Test]
        public void DoNothingIfItIsNotInBatchmode()
        {
            var target = new TestResultContext(AllGreen);

            target.ExitEditorIfBatchmode(_environment);

            Assert.That(_environment.Log(), Is.Empty);
        }
    }
}