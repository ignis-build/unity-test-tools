using System.IO;
using Ignis.Unity.Test.Tools.Runners;
using NUnit.Framework;

namespace Ignis.Unity.Test.Tools.Tests.Runners
{
    public sealed class TestJobTest
    {
        private static bool _isCalled;
        private TestJob _job;
        private MemoryStream _stream;

        [SetUp]
        public void SetUp()
        {
            _job = new TestJob();
            _stream = new MemoryStream();
            _isCalled = false;
        }

        [TearDown]
        public void TearDown()
        {
            _stream.Dispose();
        }

        [Test]
        public void TestSaveAndLoad()
        {
            _job.BindToTest("test1");

            _job.Save(_stream);
            _stream.Position = 0;

            var result = TestJob.Load(_stream);

            Assert.AreEqual("test1", result.TestId);
        }

        [Test]
        public void TestSaveAndLoadWithCallback()
        {
            _job.BindToTest("test1");
            _job.WithCallback(Callback);

            _job.Save(_stream);
            _stream.Position = 0;

            var result = TestJob.Load(_stream);
            result.InvokeCallback(null);

            Assert.That(_isCalled, Is.True);
        }

        [Test]
        public void DeleteSerializedFileAfterLoading()
        {
            _job.Save();
            TestJob.Load();

            var file = new TestJobFile();

            Assert.That(file.Exits(), Is.False);
        }

        [Test]
        public void NullWhenThereIsNoSerializedFile()
        {
            var file = new TestJobFile();
            file.Delete();
            
            Assert.That(TestJob.Load(), Is.Null);
        }

        private static void Callback(TestResultContext obj)
        {
            _isCalled = true;
        }
    }
}