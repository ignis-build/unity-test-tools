using System;
using System.Collections;

namespace Ignis.Unity.Test.Tools.Tests.Runners
{
    public static class Wait
    {
        private const int DefaultTimeout = 120;

        public static IEnumerator Until(Func<bool> predicate, int timeoutFrames = DefaultTimeout)
        {
            return WaitUntil(Coroutine(), timeoutFrames);

            IEnumerator Coroutine()
            {
                while (!predicate()) yield return null;
            }
        }

        private static IEnumerator WaitUntil(IEnumerator coroutine, int timeoutFrames = DefaultTimeout)
        {
            var frames = 0;
            while (coroutine.MoveNext())
            {
                yield return coroutine.Current;
                frames++;
                if (frames >= timeoutFrames) throw new InvalidOperationException("Timeout!");
            }
        }
    }
}