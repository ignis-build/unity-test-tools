using System.Collections;
using Ignis.Unity.Test.Tools.Runners;
using Ignis.Unity.Test.Tools.Tests.Runners.Jobs;
using NUnit.Framework;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine.TestTools;

namespace Ignis.Unity.Test.Tools.Tests.Runners
{
    public sealed class UnityTestRunnerTest
    {
        private static bool _completed;
        private UnityTestRunner _target;
        private TestCase _testCase;

        [SetUp]
        public void SetUp()
        {
            _testCase = new TestCase();
            var fakeRunner = new FakeTestRunnerApi(WaitTestCallbacks.Instance,
                new[] { _testCase });
            _target = new UnityTestRunner(
                fakeRunner,
                WaitTestCallbacks.Instance);

            _completed = false;
        }

        [UnityTest]
        public IEnumerator TestExecute()
        {
            _target.Execute(new ExecutionSettings());

            yield return Wait.Until(() => _testCase.IsCompleted);

            Assert.That(_testCase.IsCompleted, Is.True);
        }

        [UnityTest]
        public IEnumerator TestExecuteWithCallback()
        {
            _target.Execute(new ExecutionSettings())
                .WithCallback(Callback);

            yield return Wait.Until(() => _completed);

            Assert.That(_completed, Is.True);
        }

        private static void Callback(TestResultContext obj)
        {
            _completed = true;
        }
    }
}