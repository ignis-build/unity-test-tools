using Ignis.Unity.Test.Tools.Runners;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;

// ReSharper disable once CheckNamespace
public static class Test
{
    [MenuItem("Test/Edit mode")]
    public static void EditMode()
    {
        Run(TestMode.EditMode);
    }

    [MenuItem("Test/Play mode")]
    public static void PlayMode()
    {
        Run(TestMode.PlayMode);
    }

    private static void Run(TestMode mode)
    {
        var runner = new UnityTestRunner();
        runner.Execute(new ExecutionSettings(new Filter { testMode = mode }))
            .WithCallback(context =>
            {
                context.PrintSummary();
                context.ToJUnitReport().Save();

                context.ExitEditorIfBatchmode();
            });
    }
}