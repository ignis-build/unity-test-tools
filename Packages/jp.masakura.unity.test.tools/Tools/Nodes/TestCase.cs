using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Nodes
{
    public sealed class TestCase
    {
        private readonly ITestResultAdaptor _result;

        private TestCase(ITestResultAdaptor result)
        {
            _result = result;
        }

        private string Name => _result.Test.Name;
        private string ClassName => _result.Test.Parent.FullName;
        private double Time => _result.Duration;
        private ITestCaseChild Child => ITestCaseChild.Create(_result);

        public static TestCase From(ITestResultAdaptor result)
        {
            return new TestCase(result);
        }

        public XElement Xml()
        {
            return new XElement("testcase",
                new XAttribute("name", Name),
                new XAttribute("classname", ClassName),
                new XAttribute("time", Time),
                Child.Xml());
        }
    }
}