using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Nodes
{
    public sealed class TestSuites
    {
        private readonly ITestResultAdaptor _result;

        private TestSuites(ITestResultAdaptor result)
        {
            _result = result;
        }

        private IEnumerable<TestSuite> Children =>
            _result.Children
                .Where(result => result.Test.IsTestAssembly)
                .Select(TestSuite.From);

        public static TestSuites From(ITestResultAdaptor result)
        {
            return new TestSuites(result);
        }

        public XElement Xml()
        {
            return new XElement("testsuites",
                Children.Select(child => child.Xml()));
        }
    }
}