using System.Xml.Linq;

namespace Ignis.Unity.Test.Tools.Nodes
{
    public sealed class PassTestCaseChild : ITestCaseChild
    {
        private PassTestCaseChild()
        {
        }

        public static PassTestCaseChild Instance { get; } = new();

        public XElement Xml()
        {
            return null;
        }
    }
}