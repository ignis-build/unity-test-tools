using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Nodes
{
    internal interface ITestCaseChild
    {
        XElement Xml();

        public static ITestCaseChild Create(ITestResultAdaptor result)
        {
            return FailureTestCaseChild.Create(result) ??
                   SkipTestCaseChild.Create(result) ??
                   (ITestCaseChild)PassTestCaseChild.Instance;
        }
    }
}