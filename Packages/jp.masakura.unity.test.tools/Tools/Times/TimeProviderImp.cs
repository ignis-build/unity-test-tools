using System;

namespace Ignis.Unity.Test.Tools.Times
{
    internal sealed class TimeProviderImp : TimeProvider
    {
        private TimeProviderImp()
        {
        }

        public new static TimeProviderImp Instance { get; } = new();

        public override DateTimeOffset GetLocalNow()
        {
            return DateTimeOffset.Now;
        }
    }
}