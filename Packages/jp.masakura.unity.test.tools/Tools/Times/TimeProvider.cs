using System;

namespace Ignis.Unity.Test.Tools.Times
{
    public abstract class TimeProvider
    {
        public abstract DateTimeOffset GetLocalNow();

        public static TimeProvider Instance => TimeProviderImp.Instance;
    }
}