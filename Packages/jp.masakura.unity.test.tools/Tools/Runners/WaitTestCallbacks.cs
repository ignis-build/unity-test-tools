using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Runners
{
    internal sealed class WaitTestCallbacks : ICallbacks
    {
        private readonly TestJobCollection _jobs = new();

        private WaitTestCallbacks()
        {
        }

        public static WaitTestCallbacks Instance { get; } = new();

        void ICallbacks.RunStarted(ITestAdaptor testsToRun)
        {
            var job = TestJob.Load();
            job.BindToTest(testsToRun);
            _jobs.Add(job);
        }

        void ICallbacks.RunFinished(ITestResultAdaptor result)
        {
            var job = _jobs.ByTestId(result.Test.Id);

            job?.InvokeCallback(result);
        }

        void ICallbacks.TestStarted(ITestAdaptor test)
        {
        }

        void ICallbacks.TestFinished(ITestResultAdaptor result)
        {
        }

        public TestJob Register()
        {
            var job = new TestJob();
            job.Save();
            return job;
        }
    }
}