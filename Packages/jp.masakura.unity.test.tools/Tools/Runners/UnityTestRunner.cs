using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Runners
{
    /// <summary>
    ///     Unity test runner. (<see cref="TestRunnerApi" /> wrapper)
    /// </summary>
    public sealed class UnityTestRunner
    {
        private readonly ITestRunnerApi _runner;
        private readonly WaitTestCallbacks _waiter;

        public UnityTestRunner() : this(new TestRunnerApiAdapter(), WaitTestCallbacks.Instance)
        {
        }

        internal UnityTestRunner(ITestRunnerApi runner, WaitTestCallbacks waiter)
        {
            _runner = runner;
            _waiter = waiter;
        }

        /// <summary>
        ///     Coroutine to execute test and wait for completion.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns>Task include test results.</returns>
        /// <remarks>
        ///     <example>
        ///         <code>
        /// var runner = new UnityTestRunner();
        /// runner.Execute(new ExecutionSettings(/* ... */))
        ///     .WithCallback(result => result.PrintSummary());
        /// </code>
        ///     </example>
        /// </remarks>
        public TestJob Execute(ExecutionSettings settings)
        {
            _runner.Execute(settings);
            return _waiter.Register();
        }
    }
}