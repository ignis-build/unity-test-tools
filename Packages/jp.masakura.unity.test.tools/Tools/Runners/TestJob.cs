using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Runners
{
    [Serializable]
    public sealed class TestJob
    {
        private readonly List<Action<TestResultContext>> _callbacks = new();

        public string TestId { get; private set; }

        public void WithCallback(Action<TestResultContext> callback)
        {
            _callbacks.Add(callback);
            Save();
        }

        internal void BindToTest(ITestAdaptor test)
        {
            BindToTest(test.Id);
        }

        internal void BindToTest(string testId)
        {
            TestId = testId;
        }

        internal void InvokeCallback(ITestResultAdaptor result)
        {
            var context = new TestResultContext(result);
            foreach (var callback in _callbacks) callback(context);
        }

        internal void Save()
        {
            var file = new TestJobFile();
            using var stream = file.Create();
            Save(stream);
        }

        internal void Save(Stream stream)
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, this);
        }

        internal static TestJob Load()
        {
            var file = new TestJobFile();

            using var stream = file.OpenRead();
            if (stream == null) return null;

            var job = Load(stream);
            file.Delete();
            return job;
        }

        internal static TestJob Load(Stream stream)
        {
            var formatter = new BinaryFormatter();
            return (TestJob)formatter.Deserialize(stream);
        }

        public override string ToString()
        {
            return $"[{TestId}] ({_callbacks})";
        }
    }
}