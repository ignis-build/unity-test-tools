using System.IO;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Runners
{
    internal sealed class TestJobFile
    {
        private readonly string _path = Path.Combine(
            Path.GetDirectoryName(Application.dataPath)!,
            "Temp",
            "test-job"
        );

        public Stream OpenRead()
        {
            if (!Exits()) return null;

            return File.OpenRead(_path);
        }

        public Stream Create()
        {
            return File.Create(_path);
        }

        public bool Exits()
        {
            return File.Exists(_path);
        }

        public void Delete()
        {
            if (Exits()) File.Delete(_path);
        }
    }
}