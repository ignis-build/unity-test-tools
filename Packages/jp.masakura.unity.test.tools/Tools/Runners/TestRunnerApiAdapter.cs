using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Runners
{
    internal sealed class TestRunnerApiAdapter : ITestRunnerApi
    {
        private readonly TestRunnerApi _runner = ScriptableObject.CreateInstance<TestRunnerApi>();

        public void Execute(ExecutionSettings settings)
        {
            _runner.Execute(settings);
        }
    }
}