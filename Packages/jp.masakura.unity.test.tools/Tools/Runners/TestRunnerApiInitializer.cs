using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Runners
{
    [InitializeOnLoad]
    internal static class TestRunnerApiInitializer
    {
        static TestRunnerApiInitializer()
        {
            var runner = ScriptableObject.CreateInstance<TestRunnerApi>();
            runner.RegisterCallbacks(WaitTestCallbacks.Instance, int.MinValue);
        }
    }
}