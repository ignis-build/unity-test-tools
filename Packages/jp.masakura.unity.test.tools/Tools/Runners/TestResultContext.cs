using System;
using System.Threading.Tasks;
using Ignis.Unity.Test.Tools.Environments;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Runners
{
    public sealed class TestResultContext
    {
        public TestResultContext(ITestResultAdaptor result)
        {
            Result = result;
        }

        public ITestResultAdaptor Result { get; }

        /// <summary>
        ///     In batch mode only, the Editor is exited asynchronously.
        /// </summary>
        public async void ExitEditorIfBatchmode()
        {
            // Roughly wait for the processing of other ICallbacks.
            await Task.Delay(TimeSpan.FromSeconds(0.5));

            ExitEditorIfBatchmode(IUnityEditorEnvironment.Instance);
        }

        internal void ExitEditorIfBatchmode(IUnityEditorEnvironment environment)
        {
            if (!environment.IsBatchMode) return;

            environment.Exit(ExitCode());
        }

        private int ExitCode()
        {
            if (Result.FailCount > 0) return 1;
            return 0;
        }
    }
}