using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Test.Tools.Runners
{
    internal sealed class TestJobCollection
    {
        private readonly List<TestJob> _jobs = new();

        public void Add(TestJob job)
        {
            _jobs.Add(job);
        }

        public TestJob ByTestId(string testId)
        {
            return _jobs.FirstOrDefault(job => job.TestId == testId);
        }
    }
}