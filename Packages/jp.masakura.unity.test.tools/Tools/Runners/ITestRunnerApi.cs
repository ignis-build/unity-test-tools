using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Runners
{
    internal interface ITestRunnerApi
    {
        void Execute(ExecutionSettings settings);
    }
}