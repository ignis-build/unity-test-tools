using Ignis.Unity.Test.Tools.Files;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    public sealed class JUnitReporter : TestReporter
    {
        private readonly IReportFile _file;

        // ReSharper disable once UnusedMember.Global
        public JUnitReporter() : this(IReportFileNameResolver.Instance)
        {
        }

        // ReSharper disable once UnusedMember.Global
        public JUnitReporter(string fileName) : this(IReportFile.FileName(fileName))
        {
        }

        private JUnitReporter(IReportFileNameResolver fileNameResolver) :
            this(IReportFile.FileName(fileNameResolver.Resolve))
        {
        }

        public JUnitReporter(IReportFile file)
        {
            _file = file;
        }

        public override void RunFinished(ITestResultAdaptor result)
        {
            result.ToJUnitReport().Save(_file);
        }
    }
}