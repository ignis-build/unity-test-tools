using System;
using Ignis.Unity.Test.Tools.Outputs;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    /// <summary>
    ///     TestReporter that display dots.
    /// </summary>
    public sealed class DotReporter : TestReporter
    {
        private readonly IOutput _output;

        // ReSharper disable once UnusedMember.Global
        public DotReporter() : this(IOutput.Unity)
        {
        }

        public DotReporter(IOutput output)
        {
            _output = output;
        }

        public override void RunStarted(ITestAdaptor test)
        {
            _output.WriteLineToConsole();
        }

        public override void TestFinished(ITestResultAdaptor result)
        {
            if (!result.IsTestCase()) return;

            _output.WriteToConsole(DotStatus(result.TestStatus));
        }

        public override void RunFinished(ITestResultAdaptor result)
        {
            _output.WriteLineToConsole();
        }

        private static string DotStatus(TestStatus status)
        {
            return status switch
            {
                TestStatus.Passed => ".",
                TestStatus.Failed => "F",
                TestStatus.Inconclusive => "I",
                TestStatus.Skipped => "S",
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
        }
    }
}