using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    internal static class TestResultAdapterExtensions
    {
        public static bool IsTestCase(this ITestResultAdaptor result)
        {
            return !(result.Test.IsTestAssembly || result.Test.IsSuite);
        }
    }
}