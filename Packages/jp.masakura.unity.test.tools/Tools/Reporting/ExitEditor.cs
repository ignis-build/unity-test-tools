using Ignis.Unity.Test.Tools.Environments;
using Ignis.Unity.Test.Tools.Runners;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    /// <summary>
    ///     Exit editor when test is finished.
    /// </summary>
    public sealed class ExitEditor : TestReporter
    {
        private readonly IUnityEditorEnvironment _environment;

        // ReSharper disable once UnusedMember.Global
        public ExitEditor() : this(IUnityEditorEnvironment.Instance)
        {
        }

        public ExitEditor(IUnityEditorEnvironment environment)
        {
            _environment = environment;
        }

        public override void RunFinished(ITestResultAdaptor result)
        {
            var context = new TestResultContext(result);
            context.ExitEditorIfBatchmode(_environment);
        }
    }
}