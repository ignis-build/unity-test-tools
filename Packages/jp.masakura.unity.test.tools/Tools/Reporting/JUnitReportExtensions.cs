using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Runners;

// ReSharper disable once CheckNamespace
namespace UnityEditor.TestTools.TestRunner.Api
{
    public static class JUnitReportExtensions
    {
        /// <summary>
        ///     Convert to JUnit style test report.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static JUnitReport ToJUnitReport(this ITestResultAdaptor result)
        {
            return JUnitReport.From(result);
        }

        /// <summary>
        ///     Convert to JUnit style test report.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static JUnitReport ToJUnitReport(this TestResultContext context)
        {
            return context.Result.ToJUnitReport();
        }
    }
}