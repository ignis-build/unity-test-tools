using System;
using Ignis.Unity.Test.Tools.Reporting;
using Ignis.Unity.Test.Tools.Runners;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace UnityEditor.TestTools.TestRunner.Api
{
    public static class TestSummaryExtensions
    {
        /// <summary>
        ///     Print test result summary.
        /// </summary>
        /// <param name="result"></param>
        // ReSharper disable once MemberCanBePrivate.Global
        public static void PrintSummary(this ITestResultAdaptor result)
        {
            result.PrintSummary(Debug.Log);
        }

        /// <summary>
        ///     Print test result summary.
        /// </summary>
        /// <param name="context"></param>
        public static void PrintSummary(this TestResultContext context)
        {
            context.Result.PrintSummary();
        }

        /// <summary>
        ///     Print test result summary.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="logger"></param>
        public static void PrintSummary(this ITestResultAdaptor result, Action<string> logger)
        {
            var summary = new TestSummary(result);
            logger($@"
Test finished.

{summary}
".Trim());
        }
    }
}