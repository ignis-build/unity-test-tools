using Ignis.Unity.Test.Tools.Outputs;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    /// <summary>
    ///     TestReporter that displays the test summary.
    /// </summary>
    public sealed class SummaryReporter : TestReporter
    {
        private readonly IOutput _output;

        // ReSharper disable once UnusedMember.Global
        public SummaryReporter() : this(IOutput.Unity)
        {
        }

        public SummaryReporter(IOutput output)
        {
            _output = output;
        }

        public override void RunFinished(ITestResultAdaptor result)
        {
            result.PrintSummary(_output.Log);
        }
    }
}