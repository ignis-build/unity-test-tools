using System.IO;
using Ignis.Unity.Test.Tools.Files;
using Ignis.Unity.Test.Tools.Nodes;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    /// <summary>
    ///     JUnit style test report.
    /// </summary>
    public sealed class JUnitReport
    {
        private readonly TestSuites _testSuites;

        private JUnitReport(TestSuites testSuites)
        {
            _testSuites = testSuites;
        }

        public static JUnitReport From(ITestResultAdaptor result)
        {
            return new JUnitReport(TestSuites.From(result));
        }

        /// <summary>
        ///     Save test report. (TestResults-junit-{ticks}.xml)
        /// </summary>
        public void Save()
        {
            var fileName = IReportFileNameResolver.Instance.Resolve();
            Save(fileName);
        }

        /// <summary>
        ///     Save test report.
        /// </summary>
        /// <param name="fileName"></param>
        // ReSharper disable once MemberCanBePrivate.Global
        public void Save(string fileName)
        {
            Save(IReportFile.FileName(fileName));
        }

        /// <summary>
        ///     Save test report.
        /// </summary>
        /// <param name="stream"></param>
        // ReSharper disable once UnusedMember.Global
        public void Save(Stream stream)
        {
            Save(IReportFile.FromStream(stream));
        }

        /// <summary>
        ///     Save test report.
        /// </summary>
        /// <param name="writer"></param>
        // ReSharper disable once MemberCanBePrivate.Global
        public void Save(TextWriter writer)
        {
            _testSuites.Xml().Save(writer);
        }

        /// <summary>
        ///     Save test report.
        /// </summary>
        /// <param name="file"></param>
        public void Save(IReportFile file)
        {
            using var writer = file.CreateWriter();
            Save(writer);
        }
    }
}