using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    public abstract class TestReporter : ICallbacks
    {
        public virtual void RunStarted(ITestAdaptor testsToRun)
        {
        }

        public virtual void RunFinished(ITestResultAdaptor result)
        {
        }

        public virtual void TestStarted(ITestAdaptor test)
        {
        }

        public virtual void TestFinished(ITestResultAdaptor result)
        {
        }
    }
}