using Ignis.Unity.Test.Tools.Tables;
using UnityEditor.TestTools.TestRunner.Api;

namespace Ignis.Unity.Test.Tools.Reporting
{
    internal sealed class TestSummary
    {
        public TestSummary(ITestResultAdaptor result)
        {
            Passed = result.PassCount;
            Failed = result.FailCount;
            Inconclusive = result.InconclusiveCount;
            Skipped = result.SkipCount;
        }

        private int Skipped { get; }
        private int Inconclusive { get; }
        private int Failed { get; }
        private int Passed { get; }
        private int Total => Passed + Failed + Inconclusive + Skipped;

        public override string ToString()
        {
            var table = new Table();
            table.AddBody(nameof(Passed), Passed);
            table.AddBody(nameof(Failed), Failed);
            table.AddBody(nameof(Inconclusive), Inconclusive);
            table.AddBody(nameof(Skipped), Skipped);

            table.AddFooter(nameof(Total), Total);

            return table.Write();
        }
    }
}