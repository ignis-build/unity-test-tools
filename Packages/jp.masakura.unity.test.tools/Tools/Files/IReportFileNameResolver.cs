namespace Ignis.Unity.Test.Tools.Files
{
    public interface IReportFileNameResolver
    {
        static IReportFileNameResolver Instance => TicksFileNameResolver.Instance;

        string Resolve();
    }
}