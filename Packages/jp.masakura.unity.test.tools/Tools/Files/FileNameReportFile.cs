using System;
using System.IO;

namespace Ignis.Unity.Test.Tools.Files
{
    internal sealed class FileNameReportFile : IReportFile
    {
        private readonly Func<string> _fileName;

        public FileNameReportFile(Func<string> fileName)
        {
            _fileName = fileName;
        }

        public TextWriter CreateWriter()
        {
            return File.CreateText(_fileName());
        }
    }
}