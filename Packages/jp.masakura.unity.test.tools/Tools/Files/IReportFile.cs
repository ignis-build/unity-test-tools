using System;
using System.IO;

namespace Ignis.Unity.Test.Tools.Files
{
    public interface IReportFile
    {
        TextWriter CreateWriter();

        static IReportFile FileName(string fileName)
        {
            return FileName(() => fileName);
        }

        static IReportFile FileName(Func<string> fileName)
        {
            return new FileNameReportFile(fileName);
        }

        static IReportFile FromStream(Stream stream)
        {
            return new StreamReportFile(stream);
        }
    }
}