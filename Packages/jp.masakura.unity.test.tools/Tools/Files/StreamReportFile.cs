using System.IO;

namespace Ignis.Unity.Test.Tools.Files
{
    internal sealed class StreamReportFile : IReportFile
    {
        private readonly Stream _stream;

        public StreamReportFile(Stream stream)
        {
            _stream = stream;
        }

        public TextWriter CreateWriter()
        {
            return new StreamWriter(_stream);
        }
    }
}