using System;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Outputs
{
    internal sealed class UnityDebugOutput : IOutput
    {
        private UnityDebugOutput()
        {
        }

        public static UnityDebugOutput Instance { get; } = new();

        public void Log(string message)
        {
            Debug.Log(message);
        }

        public void WriteToConsole(string message)
        {
            Console.Write(message);
        }
    }
}