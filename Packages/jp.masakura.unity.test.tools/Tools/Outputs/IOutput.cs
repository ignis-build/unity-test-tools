using System;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Outputs
{
    /// <summary>
    ///     Unity Debug output.
    /// </summary>
    public interface IOutput
    {
        static IOutput Unity => UnityDebugOutput.Instance;

        /// <summary>
        ///     Proxy to <see cref="Debug.Log(object)" />
        /// </summary>
        /// <param name="message"></param>
        void Log(string message);

        /// <summary>
        ///     Proxy to <see cref="Console.Write(string)" />
        /// </summary>
        /// <param name="message"></param>
        void WriteToConsole(string message);

        /// <summary>
        ///     Proxy to <see cref="Console.WriteLine()" />
        /// </summary>
        void WriteLineToConsole()
        {
            WriteToConsole(Environment.NewLine);
        }
    }
}