namespace Ignis.Unity.Test.Tools.Environments
{
    public interface IUnityEditorEnvironment
    {
        public static IUnityEditorEnvironment Instance => DefaultUnityEditorEnvironment.Instance;

        bool IsBatchMode { get; }
        void Exit(int returnValue);
    }
}