using UnityEditor;
using UnityEngine;

namespace Ignis.Unity.Test.Tools.Environments
{
    internal sealed class DefaultUnityEditorEnvironment : IUnityEditorEnvironment
    {
        private DefaultUnityEditorEnvironment()
        {
        }

        public static DefaultUnityEditorEnvironment Instance { get; } = new();

        public bool IsBatchMode => Application.isBatchMode;

        public void Exit(int returnValue)
        {
            EditorApplication.Exit(returnValue);
        }
    }
}