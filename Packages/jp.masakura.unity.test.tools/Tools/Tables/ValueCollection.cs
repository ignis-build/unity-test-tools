using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class ValueCollection : IEnumerable<object>
    {
        private readonly object[] _values;

        public ValueCollection(IEnumerable<object> values)
        {
            _values = values.ToArray();
        }

        public int Length => _values.Length;

        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            return ((IEnumerable<object>)_values).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<object>)this).GetEnumerator();
        }

        public object Get(int columnIndex)
        {
            return _values[columnIndex];
        }

        public int MaxLength()
        {
            return _values
                .Select(value => value.ToString().Length)
                .Max();
        }
    }
}