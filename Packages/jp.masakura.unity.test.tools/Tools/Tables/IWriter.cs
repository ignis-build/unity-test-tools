using System.Collections.Generic;

namespace Ignis.Unity.Test.Tools.Tables
{
    internal interface IWriter
    {
        void WriteRow(IEnumerable<string> values);
    }
}