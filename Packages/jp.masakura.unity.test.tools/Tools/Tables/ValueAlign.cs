using System;

namespace Ignis.Unity.Test.Tools.Tables
{
    public class ValueAlign
    {
        private readonly Func<string, int, string> _align;

        private ValueAlign(Func<string, int, string> align)
        {
            _align = align;
        }

        public static ValueAlign Left { get; } = new((s, i) => s.PadRight(i));
        public static ValueAlign Right { get; } = new((s, i) => s.PadLeft(i));

        public string Write(object value, int totalWith)
        {
            return _align(value.ToString(), totalWith);
        }

        public static ValueAlign From(object value)
        {
            if (IsNumeric(value)) return Right;
            return Left;
        }

        private static bool IsNumeric(object value)
        {
            var typeCode = Type.GetTypeCode(value.GetType());
            return typeCode is >= TypeCode.SByte and <= TypeCode.Decimal;
        }
    }
}