namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class Table
    {
        public RowCollection Body { get; private set; } = RowCollection.Empty;
        public RowCollection Footer { get; private set; } = RowCollection.Empty;

        private RowCollection AllRows => Body.Concat(Footer);

        public void AddBody(params object[] values)
        {
            Body = Body.AddRow(values);
        }

        public void AddFooter(params object[] values)
        {
            Footer = Footer.AddRow(values);
        }

        public string Write()
        {
            var writer = new TableWriter();

            var formatter = new TableFormatter();
            formatter.Write(writer, this, Column.From(this));

            return writer.ToString();
        }

        public int MaxValueCountByRow()
        {
            return AllRows.MaxValueCount();
        }

        public ValueCollection ColumnValues(int columnIndex)
        {
            return AllRows.ColumnValues(columnIndex);
        }
    }
}