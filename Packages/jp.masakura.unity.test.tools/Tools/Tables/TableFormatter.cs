namespace Ignis.Unity.Test.Tools.Tables
{
    internal sealed class TableFormatter
    {
        // ReSharper disable once MemberCanBeMadeStatic.Global
        public void Write(IWriter writer,
            Table table,
            ColumnCollection columns)
        {
            WriteBody(writer, table, columns);
            if (table.Footer.IsEmpty) return;

            WriteSplitter(writer, columns);
            WriteFooter(writer, table, columns);
        }

        private static void WriteBody(IWriter writer,
            Table table,
            ColumnCollection columns)
        {
            table.Body.Write(writer, columns);
        }

        private static void WriteSplitter(IWriter writer, ColumnCollection columns)
        {
            writer.WriteRow(columns.Splitters());
        }

        private static void WriteFooter(IWriter writer,
            Table table,
            ColumnCollection columns)
        {
            table.Footer.Write(writer, columns);
        }
    }
}