using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class RowCollection
    {
        private readonly Row[] _rows;

        private RowCollection(IEnumerable<Row> rows)
        {
            _rows = rows.ToArray();
        }

        public static RowCollection Empty { get; } = new(Enumerable.Empty<Row>());
        public bool IsEmpty => _rows.Length <= 0;

        internal RowCollection AddRow(IEnumerable<object> values)
        {
            return new RowCollection(_rows.Concat(new[] { new Row(values) }));
        }

        public ValueCollection ColumnValues(int columnIndex)
        {
            var values = _rows.Select(row => row.Values.Get(columnIndex));
            return new ValueCollection(values);
        }

        public int MaxValueCount()
        {
            return _rows
                .Select(row => row.Values.Length)
                .Max();
        }

        internal void Write(IWriter writer, ColumnCollection columns)
        {
            foreach (var row in _rows)
                row.Write(writer, columns);
        }

        public RowCollection Concat(RowCollection other)
        {
            return new RowCollection(_rows.Concat(other._rows));
        }
    }
}