using System.Collections.Generic;

namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class Row
    {
        public Row(IEnumerable<object> values)
        {
            Values = new ValueCollection(values);
        }

        public ValueCollection Values { get; }

        internal void Write(IWriter writer, ColumnCollection columns)
        {
            writer.WriteRow(columns.Formatted(Values));
        }
    }
}