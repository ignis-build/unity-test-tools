using System.Collections.Generic;
using System.Text;

namespace Ignis.Unity.Test.Tools.Tables
{
    internal sealed class TableWriter : IWriter
    {
        private readonly StringBuilder _builder = new();

        public void WriteRow(IEnumerable<string> values)
        {
            var line = string.Join(" | ", values);
            _builder.AppendLine(line);
        }

        public override string ToString()
        {
            return _builder.ToString().Trim();
        }
    }
}