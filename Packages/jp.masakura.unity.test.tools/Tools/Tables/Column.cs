using System.Linq;

namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class Column
    {
        private readonly int _index;
        private readonly int _length;

        private Column(int index, int length)
        {
            _index = index;
            _length = length;
        }

        public object Get(ValueCollection values)
        {
            return values.Get(_index);
        }

        public string Formatted(object value)
        {
            return ValueAlign.From(value).Write(value, _length);
        }

        public static ColumnCollection From(Table table)
        {
            var columns = Enumerable.Range(0, table.MaxValueCountByRow())
                .Select(columnIndex => From(table, columnIndex));
            return new ColumnCollection(columns);
        }

        private static Column From(Table table, int columnIndex)
        {
            var values = table.ColumnValues(columnIndex);
            return new Column(columnIndex, values.MaxLength());
        }

        public override string ToString()
        {
            return $"index => {_index}, length => {_length}";
        }

        public string Splitter()
        {
            return new string('-', _length);
        }
    }
}