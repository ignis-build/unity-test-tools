using System.Collections.Generic;
using System.Linq;

namespace Ignis.Unity.Test.Tools.Tables
{
    public sealed class ColumnCollection
    {
        private readonly Column[] _columns;

        public ColumnCollection(IEnumerable<Column> columns)
        {
            _columns = columns.ToArray();
        }

        public IEnumerable<string> Formatted(ValueCollection values)
        {
            return _columns.Select(column => column.Formatted(column.Get(values)));
        }

        public IEnumerable<string> Splitters()
        {
            return _columns.Select(column => column.Splitter());
        }
    }
}