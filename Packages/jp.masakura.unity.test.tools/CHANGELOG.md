# CHANGELOG
## 0.4.2
- FIX: Test code is distributed and executed in the user's environment #38
- FIX: ExitApplication() might be called before the cleanup process after the test #37


## 0.4.1
- FIX: As usual, the process cannot be executed after the completion of the PlayMode test #33


## 0.4.0
- add `UnityTestRunner` to easily apply post-test processing


## 0.2.0
- add junit reporter
- add summary reporter
- add dot reporter
- add exit editor reporter