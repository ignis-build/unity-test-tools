# Unity Test Tools
A useful tools for testing. For example, outputting test reports in JUnit format.


## Install your unity project
### Install from npmjs
Add npmjs registry to `Packages/manifest.json` in your Unity project.

```json
{
  "scopedRegistries": [
    {
      "name": "npmjs",
      "url": "https://registry.npmjs.com",
      "scopes": ["jp.masakura.unity"]
    }
  ],
  "dependencies": {
    // ...
```

1. Open Package Manager Window with Your Unity project.
2. Click `+`, select `Add package by name...`
3. entry `jp.masakura.unity.test.tools`
4. Click `Add`


### Install from GitLab
1. Open Package Manager Window with your Unity project.
2. Click `+`, select `Add package from git URL...`
3. entry `https://gitlab.com/ignis-build/unity-test-tools.git?path=Packages/jp.masakura.unity.test.tools`
4. Click `Add`


## How to use
### Unity Test Runner
[TestRunnerApi](https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/reference-test-runner-api.html).Execute() executes the test, but it is difficult to wait for test completion because it runs jobs in the background.

Coroutine to execute test and wait for completion.

Create `Assets/Editor/Test.cs` file.

```csharp
public static class Test
{
    public static void Run()
    {
        var runner = new UnityTestRunner();
        runner.Execute(new ExecutionSettings(/* ... */))
            .WithCallback(context => {
                // The type of `context.Result` is ITestResultAdapter.
                // See also https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/reference-itest-result-adaptor.html
                Debug.Log($"{context.Result.Test.TestCaseCount} tests have been completed.");

                // Exit editor application, if only batchmode.
                context.ExitEditorIfBatchmode();
            });
    }
}
```

Execute in batchmode.

```shell
$ /unity/install/dir/Unity -batchmode -logfile - -executeMethod Test.Run
```

WARNING: Callback function must be serializable


### Unity Test Reporter
Output test results to report.

```csharp
var runner = new UnityTestRunner();
runner.Execute(new ExecutionSettings(/* ... */))
    .WithCallback(context => {
        // Save JUnit style report.
        context.Result.ToJUnitReport().Save("path/to/report.xml");

        // Print test result summary.
        context.Result.PrintSummary();
    });
```


#### JUnit style report
```csharp
// Save report.
task.Result.ToJUnitReport().Save("path/to/report.xml");

// Save report to TestResults-junit-{ticks}.xml. 
task.Result.ToJUnitReport().Save();
```

This report can be reviewed on GitLab or GitHub.

[GitLab Test reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html)

![](./images/gitlab.png)

[GitHub JUnit Report Action](https://github.com/marketplace/actions/junit-report-action)

![](./images/github.png)


#### Summary Report
```csharp
// Print test result summary.
task.Result.PrintSummary();
```

A summary of the test results is outputted as shown in the example

```
Test finished.

Passed       | 48
Failed       | 13
Inconclusive |  0
Skipped      |  1
------------ | --
Total        | 62
```


### Unity Test Callbacks Reporter
Create `Assets/Editor/Test.cs` file.

```csharp
using Ignis.Unity.Test.Tools.Reporting;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

[InitializeOnLoad]
public static Test
{
    public static void EditMode()
    {
    }

    static Test()
    {
        Runner = ScriptableObject.CreateInstance<TestRunnerApi>();
        
        // Displays the progress of the test with dots.
        Runner.RegisterCallbacks(new DotReporter());
        
        // Displays a summary of the test results.
        Runner.RegisterCallbacks(new SummaryReporter());

        // Outputs the test results in JUnit format.
        // (TestResults-junit-{ticks}.xml)
        Runner.RegisterCallbacks(new JUnitReporter());
        
        // Exits Unity Editor after the test is finished.
        Runner.RegisterCallbacks(new ExitEditor());
    }

    private static TestRunnerApi Runner { get; }

    [MenuItem("Build/Test PlayMode")]
    public static void PlayMode()
    {
        Runner.Execute(new ExecutionSettings(new Filter
        {
            testMode = TestMode.PlayMode
        }));
    }
}
```

Select `Build/Test PlayMode` from the Unity Editor menu.

Or execute from the command line.

```shell
$ /unity/install/dir/Unity -batchmode -logfile - -executeMethod Test.PlayMode
```


## License
[MIT](./LICENSE)
